<?php declare(strict_types=1);
/**
 * Banker
 *
 * A Caching library implementing psr/cache (PSR 6) and psr/simple-cache (PSR 16)
 *
 * PHP version 8+
 *
 * @package     Banker
 * @author      Timothy J. Warren <tim@timshomepage.net>
 * @copyright   2016 - 2023  Timothy J. Warren
 * @license     http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version     4.1.1
 * @link        https://git.timshomepage.net/timw4mail/banker
 */
namespace Aviat\Banker\Exception;

use Psr\Cache\CacheException as CacheExceptionInterface;
use Psr\SimpleCache\CacheException as SimpleCacheExceptionInterface;

/**
 * Exception interface for all exceptions thrown by an Implementing Library.
 */
class CacheException extends \Exception implements CacheExceptionInterface, SimpleCacheExceptionInterface {

}