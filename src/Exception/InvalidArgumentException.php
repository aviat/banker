<?php declare(strict_types=1);
/**
 * Banker
 *
 * A Caching library implementing psr/cache (PSR 6) and psr/simple-cache (PSR 16)
 *
 * PHP version 8+
 *
 * @package     Banker
 * @author      Timothy J. Warren <tim@timshomepage.net>
 * @copyright   2016 - 2023  Timothy J. Warren
 * @license     http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version     4.1.1
 * @link        https://git.timshomepage.net/timw4mail/banker
 */
namespace Aviat\Banker\Exception;

use Psr\Cache\InvalidArgumentException as IAEInterface;
use Psr\SimpleCache\InvalidArgumentException as SimpleIAEInterface;

/**
 * Exception interface for invalid cache arguments.
 *
 * Any time an invalid argument is passed into a method it must throw an
 * exception class which implements Psr\Cache\InvalidArgumentException.
 */
class InvalidArgumentException extends CacheException implements IAEInterface, SimpleIAEInterface {

	/**
	 * Constructor
	 *
	 * @param string $message
	 * @param int $code
	 * @param \Exception $previous
	 */
	public function __construct(string $message = 'Cache key must be a string.', int $code = 0, \Exception $previous = NULL)
	{
		parent::__construct($message, $code, $previous);
	}
}