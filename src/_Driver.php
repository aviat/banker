<?php declare(strict_types=1);
/**
 * Banker
 *
 * A Caching library implementing psr/cache (PSR 6) and psr/simple-cache (PSR 16)
 *
 * PHP version 8+
 *
 * @package     Banker
 * @author      Timothy J. Warren <tim@timshomepage.net>
 * @copyright   2016 - 2023  Timothy J. Warren
 * @license     http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version     4.1.1
 * @link        https://git.timshomepage.net/timw4mail/banker
 */
namespace Aviat\Banker;

use Aviat\Banker\Driver\AbstractDriver;

/**
 * Private trait for shared driver-related functionality
 */
trait _Driver {
	/**
	 * Driver class for handling the chosen caching backend
	 */
	private AbstractDriver $driver;

	/**
	 * Instantiate the appropriate cache backend based on the config
	 */
	protected function loadDriver(array $driverConfig = []): AbstractDriver
	{
		$driver = ucfirst(strtolower($driverConfig['driver'] ?? 'null'));
		$class = __NAMESPACE__ . "\\Driver\\{$driver}Driver";

		$driverConfig['connection'] = $driverConfig['connection'] ?? [];
		$driverConfig['options'] = $driverConfig['options'] ?? [];

		// @phpstan-ignore-next-line
		return new $class($driverConfig['connection'], $driverConfig['options']);
	}
}