<?php declare(strict_types=1);
/**
 * Banker
 *
 * A Caching library implementing psr/cache (PSR 6) and psr/simple-cache (PSR 16)
 *
 * PHP version 8+
 *
 * @package     Banker
 * @author      Timothy J. Warren <tim@timshomepage.net>
 * @copyright   2016 - 2023  Timothy J. Warren
 * @license     http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version     4.1.1
 * @link        https://git.timshomepage.net/timw4mail/banker
 */
namespace Aviat\Banker;

use DateInterval;
use Psr\Log\{LoggerInterface, LoggerAwareInterface};
use Psr\SimpleCache;

/**
 * Implements PSR-16 (SimpleCache)
 */
class Teller implements LoggerAwareInterface, SimpleCache\CacheInterface {
	use _Driver;
	use KeyValidateTrait;
	use LoggerTrait;

	/**
	 * Set up the cache backend
	 *
	 * @param array $config
	 * @param LoggerInterface|null $logger
	 */
	public function __construct(array $config, ?LoggerInterface $logger = NULL)
	{
		$this->driver = $this->loadDriver($config);

		if ($logger !== NULL)
		{
			$this->setLogger($logger);
		}
	}

	/**
	 * Wipes clean the entire cache's keys.
	 *
	 * @return bool True on success and false on failure.
	 */
	public function clear(): bool
	{
		return $this->driver->flush();
	}

	/**
	 * Fetches a value from the cache.
	 *
	 * @param string $key     The unique key of this item in the cache.
	 * @param mixed  $default Default value to return if the key does not exist.
	 *
	 * @return mixed The value of the item from the cache, or $default in case of cache miss.
	 *
	 * @throws SimpleCache\InvalidArgumentException
	 *   MUST be thrown if the $key string is not a legal value.
	 */
	public function get(string $key, mixed $default = null): mixed
	{
		$this->validateKey($key);

		return ($this->driver->exists($key)) ? $this->driver->get($key) : $default;
	}

	/**
	 * Persists data in the cache, uniquely referenced by a key with an optional expiration TTL time.
	 *
	 * @param string                 $key   The key of the item to store.
	 * @param mixed                  $value The value of the item to store, must be serializable.
	 * @param null|int|DateInterval $ttl   Optional. The TTL value of this item. If no value is sent and
	 *                                      the driver supports TTL then the library may set a default value
	 *                                      for it or let the driver take care of that.
	 *
	 * @return bool True on success and false on failure.
	 *
	 * @throws SimpleCache\InvalidArgumentException
	 *   MUST be thrown if the $key string is not a legal value.
	 */
	public function set(string $key, mixed $value, null|int|DateInterval $ttl = null): bool
	{
		$this->validateKey($key);

		return $this->driver->set($key, $value, $ttl);
	}

	/**
	 * Delete an item from the cache by its unique key.
	 *
	 * @param string $key The unique cache key of the item to delete.
	 *
	 * @return bool True if the item was successfully removed. False if there was an error.
	 *
	 * @throws SimpleCache\InvalidArgumentException
	 *   MUST be thrown if the $key string is not a legal value.
	 */
	public function delete(string $key): bool
	{
		$this->validateKey($key);

		return $this->driver->delete($key);
	}

	/**
	 * Obtains multiple cache items by their unique keys.
	 *
	 * @param iterable $keys    A list of keys that can obtained in a single operation.
	 * @param mixed    $default Default value to return for keys that do not exist.
	 *
	 * @return iterable A list of key => value pairs. Cache keys that do not exist or are stale will have $default as value.
	 *
	 * @throws SimpleCache\InvalidArgumentException
	 *   MUST be thrown if $keys is neither an array nor a Traversable,
	 *   or if any of the $keys are not a legal value.
	 */
	public function getMultiple(iterable $keys, mixed $default = null): iterable
	{
		$keys = (array)$keys;
		$this->validateKeys($keys);
		$foundValues = $this->driver->getMultiple($keys);
		$foundKeys = array_keys($foundValues);

		// If all the values are found, just return them
		if ($keys === $foundKeys)
		{
			return $foundValues;
		}

		// Otherwise, return a default value for missing keys
		$result = $foundValues;
		foreach (array_diff($keys, $foundKeys) as $key)
		{
			$result[$key] = $default;
		}

		return $result;
	}

	/**
	 * Persists a set of key => value pairs in the cache, with an optional TTL.
	 *
	 * @param iterable               $values A list of key => value pairs for a multiple-set operation.
	 * @param null|int|DateInterval $ttl    Optional. The TTL value of this item. If no value is sent and
	 *                                       the driver supports TTL then the library may set a default value
	 *                                       for it or let the driver take care of that.
	 *
	 * @return bool True on success and false on failure.
	 *
	 * @throws SimpleCache\InvalidArgumentException
	 *   MUST be thrown if $values is neither an array nor a Traversable,
	 *   or if any of the $values are not a legal value.
	 */
	public function setMultiple(iterable $values, null|int|DateInterval $ttl = null): bool
	{
		$this->validateKeys($values, TRUE);

		return ($ttl === NULL)
			? $this->driver->setMultiple((array)$values)
			: $this->driver->setMultiple((array)$values, $ttl);
	}

	/**
	 * Deletes multiple cache items in a single operation.
	 *
	 * @param iterable $keys A list of string-based keys to be deleted.
	 *
	 * @return bool True if the items were successfully removed. False if there was an error.
	 *
	 * @throws SimpleCache\InvalidArgumentException
	 *   MUST be thrown if $keys is neither an array nor a Traversable,
	 *   or if any of the $keys are not a legal value.
	 */
	public function deleteMultiple(iterable $keys): bool
	{
		$this->validateKeys($keys);

		return $this->driver->deleteMultiple((array)$keys);
	}

	/**
	 * Determines whether an item is present in the cache.
	 *
	 * NOTE: It is recommended that has() is only to be used for cache warming type purposes
	 * and not to be used within your live applications operations for get/set, as this method
	 * is subject to a race condition where your has() will return true and immediately after,
	 * another script can remove it making the state of your app out of date.
	 *
	 * @param string $key The cache item key.
	 *
	 * @return bool
	 *
	 * @throws SimpleCache\InvalidArgumentException
	 *   MUST be thrown if the $key string is not a legal value.
	 */
	public function has(string $key): bool
	{
		$this->validateKey($key);

		return $this->driver->exists($key);
	}
}