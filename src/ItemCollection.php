<?php declare(strict_types=1);
/**
 * Banker
 *
 * A Caching library implementing psr/cache (PSR 6) and psr/simple-cache (PSR 16)
 *
 * PHP version 8+
 *
 * @package     Banker
 * @author      Timothy J. Warren <tim@timshomepage.net>
 * @copyright   2016 - 2023  Timothy J. Warren
 * @license     http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version     4.1.1
 * @link        https://git.timshomepage.net/timw4mail/banker
 */
namespace Aviat\Banker;

use Psr\Cache\CacheItemInterface;

use ArrayIterator;
use JsonSerializable;

/**
 * Collection of Psr\Cache\CacheItemInterface objects to be returned by getItems
 *
 * @see http://php.net/manual/en/class.arrayiterator.php
 * @see http://php.net/manual/en/class.jsonserializable.php
 * @extends ArrayIterator<string, CacheItemInterface>
 */
class ItemCollection extends ArrayIterator implements JsonSerializable {

	/**
	 * The raw CacheItemInterface objects
	 *
	 * @var CacheItemInterface[]
	 */
	protected array $items = [];

	/**
	 * Create the collection object from the raw
	 * CacheItemInterface array
	 *
	 * @param CacheItemInterface[] $items - array of CacheItemInterface objects
	 * @param int $flags - flags
	 */
	public function __construct(array $items = [], int $flags = 0)
	{
		parent::__construct($items, $flags);
		$this->items = $items;
	}

	/**
	 * Specify what data to serialize when using `json_encode`
	 *
	 * @return CacheItemInterface[] - The full set of data to be serialized
	 */
	public function jsonSerialize(): array
	{
		return $this->items;
	}
}