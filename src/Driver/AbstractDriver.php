<?php declare(strict_types=1);
/**
 * Banker
 *
 * A Caching library implementing psr/cache (PSR 6) and psr/simple-cache (PSR 16)
 *
 * PHP version 8+
 *
 * @package     Banker
 * @author      Timothy J. Warren <tim@timshomepage.net>
 * @copyright   2016 - 2023  Timothy J. Warren
 * @license     http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version     4.1.1
 * @link        https://git.timshomepage.net/timw4mail/banker
 */
namespace Aviat\Banker\Driver;

use Aviat\Banker\LoggerTrait;
use Aviat\Banker\KeyValidateTrait;
use DateInterval;
use Psr\Log\LoggerAwareInterface;

/**
 * Base class for cache backends
 */
abstract class AbstractDriver implements DriverInterface, LoggerAwareInterface {

	use KeyValidateTrait;
	use LoggerTrait;

	/**
	 * Data to be stored later
	 *
	 * @var array
	 */
	protected array $deferred = [];

	/**
	 * Common constructor interface for driver classes
	 */
	abstract public function __construct();

	/**
	 * Retrieve a set of values by their cache key
	 *
	 * @param string[] $keys
	 * @return array
	 */
	public function getMultiple(array $keys = []): array
	{
		$this->validateKeys($keys);

		$output = [];

		foreach ($keys as $key)
		{
			if ($this->exists($key))
			{
				$output[$key] = $this->get($key);
			}
		}

		return $output;
	}

	/**
	 * Set multiple cache values
	 *
	 * @param array $items
	 * @param DateInterval|int|null $expires
	 * @return bool
	 */
	public function setMultiple(array $items, DateInterval|int|null $expires = NULL): bool
	{
		$this->validateKeys($items, TRUE);

		$setResults = [];
		foreach ($items as $k => $v)
		{
			$setResults[] = ($expires === NULL)
				? $this->set($k, $v)
				: $this->set($k, $v, $expires);
		}

		// Only return true if all the results are true
		return array_reduce($setResults, fn ($carry, $item) => $item && $carry, TRUE);
	}
}