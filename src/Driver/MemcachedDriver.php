<?php declare(strict_types=1);
/**
 * Banker
 *
 * A Caching library implementing psr/cache (PSR 6) and psr/simple-cache (PSR 16)
 *
 * PHP version 8+
 *
 * @package     Banker
 * @author      Timothy J. Warren <tim@timshomepage.net>
 * @copyright   2016 - 2023  Timothy J. Warren
 * @license     http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version     4.1.1
 * @link        https://git.timshomepage.net/timw4mail/banker
 */
namespace Aviat\Banker\Driver;

use Aviat\Banker\Exception\CacheException;

use Aviat\Banker\Exception\InvalidArgumentException;
use DateInterval;
use Memcached;
use MemcachedException;

/**
 * Memcached cache backend
 */
class MemcachedDriver extends AbstractDriver {

	/**
	 * The Memcached connection
	 */
	private Memcached $conn;

	/**
	 * Driver for PHP Memcache extension
	 *
	 * @codeCoverageIgnore
	 * @param array $config
	 * @param array $options
	 * @throws CacheException
	 */
	public function __construct(
		array $config = ['host' => '127.0.0.1', 'port' => '11211'],
		array $options = []
	)
	{
		if ( ! class_exists('Memcached'))
		{
			throw new CacheException('Memcached driver requires memcached extension');
		}

		try
		{
			$this->conn = new Memcached();
			$this->conn->setOption(Memcached::OPT_BINARY_PROTOCOL, true);
			$this->conn->addServer($config['host'], (int) $config['port']);

			if ( ! empty($options))
			{
				$this->conn->setOptions($options);
			}
		}
		catch (MemcachedException $e)
		{
			// Rewrite MemcachedException as a CacheException to
			// match the requirements of the interface
			throw new CacheException($e->getMessage(), $e->getCode(), $e);
		}
	}

	/**
	 * Disconnect from memcached server
	 * @codeCoverageIgnore
	 */
	public function __destruct()
	{
		$this->conn->quit();
	}

	/**
	 * See if a key currently exists in the cache
	 *
	 * @param string $key
	 * @return bool
	 */
	public function exists(string $key): bool
	{
		$this->conn->get($key);
		$resultFlag = $this->conn->getResultCode();

		return ($resultFlag !== Memcached::RES_NOTFOUND);
	}

	/**
	 * Get the value for the selected cache key
	 *
	 * @param string $key
	 * @return mixed
	 */
	public function get(string $key): mixed
	{
		return $this->conn->get($key);
	}

	/**
	 * Retrieve a set of values by their cache key
	 *
	 * @param string[] $keys
	 * @return array
	 */
	public function getMultiple(array $keys = []): array
	{
		$this->validateKeys($keys);

		$response = $this->conn->getMulti($keys);
		return (is_array($response)) ? $response : [];
	}

	/**
	 * Set a cached value
	 *
	 * @param string $key
	 * @param mixed $value
	 * @param int|DateInterval|null $expires
	 * @return bool
	 * @throws InvalidArgumentException
	 */
	public function set(string $key, mixed $value, int|DateInterval|null $expires = NULL): bool
	{
		$this->validateKey($key);

		if ($expires instanceof DateInterval)
		{
			$expires = time() + $expires->s;
		}

		return ($expires === NULL)
			? $this->conn->set($key, $value)
			: $this->conn->set($key, $value, $expires);
	}

	/**
	 * Set multiple cache values
	 *
	 * @param array $items
	 * @param DateInterval|int|null $expires
	 * @return bool
	 */
	public function setMultiple(array $items, DateInterval|int|null $expires = NULL): bool
	{
		$this->validateKeys($items, TRUE);

		if ($expires instanceof DateInterval)
		{
			$expires = $expires->s;
		}

		return ($expires === NULL)
			? $this->conn->setMulti($items)
			: $this->conn->setMulti($items, $expires);
	}

	/**
	 * Remove an item from the cache
	 *
	 * @param string $key
	 * @return boolean
	 */
	public function delete(string $key): bool
	{
		return $this->conn->delete($key);
	}

	/**
	 * Remove multiple items from the cache
	 *
	 * @param string[] $keys
	 * @return boolean
	 */
	public function deleteMultiple(array $keys = []): bool
	{
		$this->validateKeys($keys);

		$deleted = $this->conn->deleteMulti($keys);

		if (is_array($deleted))
		{
			foreach ($deleted as $key => $status)
			{
				if ($status !== TRUE)
				{
					return FALSE;
				}
			}

			return TRUE;
		}
	}

	/**
	 * Empty the cache
	 *
	 * @return boolean
	 */
	public function flush(): bool
	{
		return $this->conn->flush();
	}

	/**
	 * Set the specified key to expire at the given time
	 *
	 * @param string $key
	 * @param int $expires
	 * @return boolean
	 */
	public function expiresAt(string $key, int $expires): bool
	{
		if ($this->exists($key))
		{
			return $this->conn->touch($key, $expires);
		}

		$this->getLogger()->log('warning','Tried to set expiration on a key that does not exist');

		return FALSE;
	}
}
