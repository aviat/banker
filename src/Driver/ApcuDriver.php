<?php declare(strict_types=1);
/**
 * Banker
 *
 * A Caching library implementing psr/cache (PSR 6) and psr/simple-cache (PSR 16)
 *
 * PHP version 8+
 *
 * @package     Banker
 * @author      Timothy J. Warren <tim@timshomepage.net>
 * @copyright   2016 - 2023  Timothy J. Warren
 * @license     http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version     4.1.1
 * @link        https://git.timshomepage.net/timw4mail/banker
 */
namespace Aviat\Banker\Driver;

use Aviat\Banker\Exception\CacheException;

use DateInterval;
use function apcu_clear_cache;
use function apcu_delete;
use function apcu_exists;
use function apcu_fetch;
use function apcu_store;

/**
 * Memcached cache backend
 */
class ApcuDriver extends AbstractDriver {

	/**
	 * Constructor
	 *
	 * @throws CacheException
	 * @codeCoverageIgnore
	 */
	public function __construct()
	{
		if ( ! extension_loaded('apcu'))
		{
			throw new CacheException('This driver requires the APCU extension');
		}
	}

	/**
	 * See if a key currently exists in the cache
	 *
	 * @param string $key
	 * @return bool
	 */
	public function exists(string $key): bool
	{
		return apcu_exists($key) !== FALSE;
	}

	/**
	 * Get the value for the selected cache key
	 *
	 * @param string $key
	 * @return mixed
	 */
	public function get(string $key): mixed
	{
		return apcu_fetch($key);
	}

	/**
	 * Retrieve a set of values by their cache key
	 *
	 * @param string[] $keys
	 * @return array
	 */
	public function getMultiple(array $keys = []): array
	{
		$this->validateKeys($keys);

		$status = FALSE;
		return (array)apcu_fetch($keys, $status);
	}

	/**
	 * Set a cached value
	 *
	 * @param string $key
	 * @param mixed $value
	 * @param int|DateInterval|null $expires
	 * @return bool
	 */
	public function set(string $key, mixed $value, int|DateInterval|null $expires = NULL): bool
	{
		$ttl = $this->getTTLFromExpiration($expires);

		return apcu_store($key, $value, $ttl);
	}

	/**
	 * Set multiple cache values
	 *
	 * @param array $items
	 * @param DateInterval|int|null $expires
	 * @return bool
	 */
	public function setMultiple(array $items, DateInterval|int|null $expires = NULL): bool
	{
		$this->validateKeys($items, TRUE);

		$errorKeys = ($expires === NULL)
			? apcu_store($items)
			: apcu_store($items, NULL, $this->getTTLFromExpiration($expires));

		return empty($errorKeys);
	}

	/**
	 * Remove an item from the cache
	 *
	 * @param string $key
	 * @return boolean
	 */
	public function delete(string $key): bool
	{
		return apcu_delete($key);
	}

	/**
	 * Remove multiple items from the cache
	 *
	 * @param string[] $keys
	 * @return boolean
	 */
	public function deleteMultiple(array $keys = []): bool
	{
		$this->validateKeys($keys);

		$failedToDelete = apcu_delete($keys);
		return empty($failedToDelete);
	}

	/**
	 * Empty the cache
	 *
	 * @return boolean
	 */
	public function flush(): bool
	{
		return apcu_clear_cache();
	}

	/**
	 * Set the specified key to expire at the given time
	 *
	 * @param string $key
	 * @param int $expires
	 * @return boolean
	 */
	public function expiresAt(string $key, int $expires): bool
	{
		if ($this->exists($key))
		{
			$value = $this->get($key);
			$ttl = $this->getTTLFromExpiration($expires);
			return apcu_store($key, $value, $ttl);
		}

		$this->getLogger()->log('warning', 'Tried to set expiration on a key that does not exist');

		return FALSE;
	}

	/**
	 * Convert expiration date argument into TTL argument
	 *
	 * @param DateInterval|int|null $expires
	 * @return int
	 */
	protected function getTTLFromExpiration(DateInterval|int|null $expires): int
	{
		if ($expires instanceof DateInterval)
		{
			return $expires->s;
		}

		$ttl = (int)$expires - time();

		return ($ttl < 0) ? 0 : $ttl;
	}
}
