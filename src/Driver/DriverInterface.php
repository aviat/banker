<?php declare(strict_types=1);
/**
 * Banker
 *
 * A Caching library implementing psr/cache (PSR 6) and psr/simple-cache (PSR 16)
 *
 * PHP version 8+
 *
 * @package     Banker
 * @author      Timothy J. Warren <tim@timshomepage.net>
 * @copyright   2016 - 2023  Timothy J. Warren
 * @license     http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version     4.1.1
 * @link        https://git.timshomepage.net/timw4mail/banker
 */
namespace Aviat\Banker\Driver;

use DateInterval;

/**
 * Interface for different cache backends
 */
interface DriverInterface {

	/**
	 * See if a key exists in the cache
	 *
	 * @param string $key
	 * @return bool
	 */
	public function exists(string $key): bool;

	/**
	 * Set a cached value
	 *
	 * @param string $key
	 * @param mixed $value
	 * @param DateInterval|int|null $expires
	 * @return bool
	 */
	public function set(string $key, mixed $value, DateInterval|int|null $expires = NULL): bool;

	/**
	 * Get the value for the selected cache key
	 *
	 * @param string $key
	 * @return mixed
	 */
	public function get(string $key): mixed;

	/**
	 * Retrieve a set of values by their cache key
	 *
	 * @param string[] $keys
	 * @return array
	 */
	public function getMultiple(array $keys = []): array;

	/**
	 * Set multiple cache values
	 *
	 * @param array $items
	 * @param int|null $expires
	 * @return bool
	 */
	public function setMultiple(array $items, ?int $expires = NULL): bool;

	/**
	 * Remove an item from the cache
	 *
	 * @param string $key
	 * @return boolean
	 */
	public function delete(string $key): bool;

	/**
	 * Remove multiple items from the cache
	 *
	 * @param string[] $keys
	 * @return boolean
	 */
	public function deleteMultiple(array $keys = []): bool;

	/**
	 * Empty the cache
	 *
	 * @return boolean
	 */
	public function flush(): bool;

	/**
	 * Set the specified key to expire at the given time
	 *
	 * @param string $key
	 * @param int $expires
	 * @return boolean
	 */
	public function expiresAt(string $key, int $expires): bool;
}