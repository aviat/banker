<?php declare(strict_types=1);
/**
 * Banker
 *
 * A Caching library implementing psr/cache (PSR 6) and psr/simple-cache (PSR 16)
 *
 * PHP version 8+
 *
 * @package     Banker
 * @author      Timothy J. Warren <tim@timshomepage.net>
 * @copyright   2016 - 2023  Timothy J. Warren
 * @license     http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version     4.1.1
 * @link        https://git.timshomepage.net/timw4mail/banker
 */
namespace Aviat\Banker;

use Psr\Log\{
	LoggerAwareTrait,
	LoggerInterface,
	LogLevel,
	NullLogger
};

/**
 * Trait for keeping track of logger objects
 */
trait LoggerTrait {

	use LoggerAwareTrait;

	/**
	 * Return the existing logger instance or
	 * a NullLogger, if no instance set
	 */
	protected function getLogger(): LoggerInterface
	{
		if ($this->logger === NULL)
		{
			$this->logger = new NullLogger();
		}
		return $this->logger;
	}

	/**
	 * Set a logger to keep track of errors
	 */
	public function setLogger(LoggerInterface $logger): void
	{
		$this->logger = $logger;

		// Set the logger for the current driver too
		if (isset($this->driver))
		{
			$this->driver->setLogger($logger);
		}
	}
}