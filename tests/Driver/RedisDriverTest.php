<?php declare(strict_types=1);
/**
 * Banker
 *
 * A Caching library implementing psr/cache (PSR 6) and psr/simple-cache (PSR 16)
 *
 * PHP version 8+
 *
 * @package     Banker
 * @author      Timothy J. Warren <tim@timshomepage.net>
 * @copyright   2016 - 2023  Timothy J. Warren
 * @license     http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version     4.1.1
 * @link        https://git.timshomepage.net/timw4mail/banker
 */
namespace Aviat\Banker\Tests\Driver;

use Aviat\Banker\Driver\RedisDriver;

class RedisDriverTest extends DriverTestBase {

	public function setup(): void
	{
		$config = [];
		if (array_key_exists('REDIS_HOST', $_ENV))
		{
			$config['scheme'] = 'tcp';
			$config['host'] = $_ENV['REDIS_HOST'];
			$config['port'] = $_ENV['REDIS_PORT'] ?? 6379;
		}

		$this->driver = new RedisDriver($config);
		$this->driver->flush();
	}

}