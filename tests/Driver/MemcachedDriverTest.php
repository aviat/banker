<?php declare(strict_types=1);
/**
 * Banker
 *
 * A Caching library implementing psr/cache (PSR 6) and psr/simple-cache (PSR 16)
 *
 * PHP version 8+
 *
 * @package     Banker
 * @author      Timothy J. Warren <tim@timshomepage.net>
 * @copyright   2016 - 2023  Timothy J. Warren
 * @license     http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version     4.1.1
 * @link        https://git.timshomepage.net/timw4mail/banker
 */
namespace Aviat\Banker\Tests\Driver;

use Aviat\Banker\Driver\MemcachedDriver;

class MemcachedDriverTest extends DriverTestBase {

	public function setUp(): void
	{
		if ( ! class_exists('Memcached'))
		{
			$this->markTestSkipped();
			return;
		}

		$config = [
			'host' => '127.0.0.1',
			'port' => 11211
		];
		if (array_key_exists('MEMCACHED_HOST', $_ENV))
		{
			$config['host'] = $_ENV['MEMCACHED_HOST'];
			$config['port'] = $_ENV['MEMCACHED_PORT'] ?? 11211;
		}

		$this->driver = new MemcachedDriver($config);
		$this->driver->flush();
	}
}