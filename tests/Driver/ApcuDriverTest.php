<?php declare(strict_types=1);
/**
 * Banker
 *
 * A Caching library implementing psr/cache (PSR 6) and psr/simple-cache (PSR 16)
 *
 * PHP version 8+
 *
 * @package     Banker
 * @author      Timothy J. Warren <tim@timshomepage.net>
 * @copyright   2016 - 2023  Timothy J. Warren
 * @license     http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version     4.1.1
 * @link        https://git.timshomepage.net/timw4mail/banker
 */
namespace Aviat\Banker\Tests\Driver;

use Aviat\Banker\Driver\ApcuDriver;

class ApcuDriverTest extends DriverTestBase {

	public function setup(): void
	{
		if ( ! extension_loaded('apcu'))
		{
			$this->markTestSkipped();
			return;
		}

		$this->driver = new ApcuDriver();
		$this->driver->flush();
	}
}
