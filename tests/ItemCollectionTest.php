<?php declare(strict_types=1);
/**
 * Banker
 *
 * A Caching library implementing psr/cache (PSR 6) and psr/simple-cache (PSR 16)
 *
 * PHP version 8+
 *
 * @package     Banker
 * @author      Timothy J. Warren <tim@timshomepage.net>
 * @copyright   2016 - 2023  Timothy J. Warren
 * @license     http://www.opensource.org/licenses/mit-license.html  MIT License
 * @version     4.1.1
 * @link        https://git.timshomepage.net/timw4mail/banker
 */
namespace Aviat\Banker\Tests;

use Aviat\Banker\ItemCollection;
use PHPUnit\Framework\TestCase;

class ItemCollectionTest extends TestCase {

	protected ItemCollection $collection;

	public function setUp(): void
	{
		$this->collection = new ItemCollection([]);
	}

	public function testJsonSerialize(): void
	{
		$this->assertEquals([], $this->collection->jsonSerialize());

		$json = json_encode($this->collection);
		$result = json_decode($json);
		$this->assertEquals([], $result);
	}
}