# Banker

A Caching library implementing the PSR-6 and PSR-16 interfaces for several common cache backends

## Cache Backends
* Apcu
* Memcached
* Redis
* Null - no persistence

### Basic Usage (SimpleCache/PSR-16)

```php
<?php
// $config is the configuration array
// $logger is an optional psr/log compatible logger
$cache = new Aviat\Banker\Teller($config, $logger);

// Get a value from the cache, returning $defaultValue
// if the value doesn't exist in the cache
$value = $cache->get($key, $defaultValue);

// Save a new value at the specified key
$saved = $cache->set($key, 'newValue');
```

### Basic Usage (Pool/PSR-6)
```php
<?php
// Create the pool
// $config is the configuration array
// $logger is an optional psr/log compatible logger
$pool = new Aviat\Banker\Pool($config, $logger);

// Grab an item from the cache
$item = $pool->getItem('foo');

// Was there a cache hit?
if ( ! $item->isHit())
{
	// ... Generation of value to cache
	$item->set($value);
	$item->save();
}
else
{
	$value = $item->get();
}
```

#### Configuration / Connection array

The config array passed to the Pool class constructor determines
which server to connect to. Regardless of the backend, the basic
structure is like so:

```php
<?php
$config = [
	'driver' => 'null', // null, apcu, redis, memcached
	'connection' => [
		// Optional (For some drivers):
		// driver setup, see below for the structure for each
		// driver
	],
	'options' => [
		// Optional:
		// Set additional driver-specific options, like persistence for
		// Memcached, or a prefix for Redis keys
	]
];
```

Below are the connection arrays for each backend:

Memcached:
```php
<?php
$config['connection'] = [
	'host' => 'localhost', // hostname or socket
	'port' => 11211,       // Port needs to be 0 if socket
	'persistent' => false, // Use persistent connection
];
```

Redis:
See [Predis](https://github.com/nrk/predis#connecting-to-redis) documentation. An empty array will connect to localhost on port 6379.

Null, Apcu:
No connection parameters