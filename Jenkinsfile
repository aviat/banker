pipeline {
	agent any
	stages {
		stage("Setup") {
			steps {
				sh 'curl -sS https://getcomposer.org/installer | php'
				sh 'rm -rf ./vendor'
				sh 'rm -f composer.lock'
				sh 'phive --no-progress install --trust-gpg-keys 67F861C3D889C656'
				sh 'php composer.phar install --ignore-platform-reqs'
			}
		}
		stage("Test PHP 8") {
			steps {
				script {
					docker.image("memcached:latest").withRun("-p 11212:11211") { c ->
						docker.image("redis:latest").withRun("-p 6380:6379") { d ->
							docker.image("php:8").withRun("-e REDIS_HOST=redis -e REDIS_PORT=11212 -e MEMCACHED_HOST=mem -e MEMCACHED_PORT=6380 --link ${d.id}:redis --link ${c.id}:mem") { p ->
								sh "sh build/docker_install.sh"
								sh "php ./vendor/bin/phpunit -c build --no-coverage --colors=never"
							}
						}
					}
				}
			}
		}
		stage("Test PHP 8.1") {
			steps {
				script {
					docker.image("memcached:latest").withRun("-p 11212:11211") { c ->
						docker.image("redis:latest").withRun("-p 6380:6379") { d ->
							docker.image("php:8.1").withRun("-e REDIS_HOST=redis -e REDIS_PORT=11212 -e MEMCACHED_HOST=mem -e MEMCACHED_PORT=6380 --link ${d.id}:redis --link ${c.id}:mem") { p ->
								sh "sh build/docker_install.sh"
								sh "php ./vendor/bin/phpunit -c build --no-coverage --colors=never"
							}
						}
					}
				}
			}
		}
		stage("Test PHP 8.2") {
			steps {
				script {
					docker.image("memcached:latest").withRun("-p 11212:11211") { c ->
						docker.image("redis:latest").withRun("-p 6380:6379") { d ->
							docker.image("php:8.2").withRun("-e REDIS_HOST=redis -e REDIS_PORT=11212 -e MEMCACHED_HOST=mem -e MEMCACHED_PORT=6380 --link ${d.id}:redis --link ${c.id}:mem") { p ->
								sh "sh build/docker_install.sh"
								sh "php ./vendor/bin/phpunit -c build --no-coverage --colors=never"
							}
						}
					}
				}
			}
		}
		stage('Code Cleanliness') {
			agent any
			steps {
				sh 'cd tools && composer install --ignore-platform-reqs && cd ..'
				sh 'mkdir -p build/logs'
				sh 'mkdir -p build/tmp'
				sh 'touch build/logs/phpstan.log'
				/* sh 'composer run-script ci:phpstan' */
				recordIssues(
					failOnError: false,
					tools: [phpStan(reportEncoding: 'UTF-8', pattern: 'build/logs/phpstan.log')]
				)
			}
		}
	}
	post {
		success {
			sh 'php composer.phar run-script coverage'
			step([
				$class: 'CloverPublisher',
				cloverReportDir: '',
				cloverReportFileName: 'build/logs/clover.xml',
			])
			junit 'build/logs/junit.xml'
		}
	}
}