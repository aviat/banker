# Changelog

## 4.1.0
* Updates for PHP 8.2 compatibility

## 4.0.0
* Updated interfaces to match newer PSR interfaces
* Updates for PHP 8.1 compatibility
* Increased required PHP version to 8

## 3.2.0
* Improved validation of cache keys
* Updated dependencies

## 3.1.0
* Added key name checks to `Pool` class

## 3.0.0
* Updated dependencies
* Increased required PHP version to 7.4
* Added SimpleCache (PSR-16) implementation in `Teller` class

## 2.0.0
* Removed `Memcache` integration, as the extension does not seem to be maintained
* Increased required PHP version to 7.1