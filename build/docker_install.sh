#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && [[ ! -e /.dockerinit ]] && exit 0

set -xe

apt-get update && apt-get install -y \
	libz-dev \
	libmemcached-dev \
	git \
	curl

docker-php-ext-install zip
pecl install apcu
pecl install memcached
pecl install pcov
docker-php-ext-enable apcu memcached pcov